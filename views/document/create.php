<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Document */

$this->title = Yii::t('docvault', 'Create New Document');
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('docvault', 'Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
