<?php
namespace diggindata\docvault\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;

class TileView extends ListView
{

    public function renderItems()
    {
        $models = $this->dataProvider->getModels();
        $keys = $this->dataProvider->getKeys();
        $rows = [];
        foreach (array_values($models) as $index => $model) {
            if($index%3==0 and $index>0)
                $rows[] = '</div><!-- row -->';
            if($index%3==0)
                $rows[] = '<div class="row">';
            $rows[] = $this->renderItem($model, $keys[$index], $index);
        }

        return implode($this->separator, $rows);

    }
}
