<?php
use yii\helpers\Html;
?>

<p>
    <?= ($model->status==0 and $model->mayModify) ? Html::a(Yii::t('docvault', 'Update Permissions'), ['update-permissions', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
</p>

<?php // yii\helpers\VarDumper::dump($model->permissions, 10, true); ?>

<table class="table table-striped table-bordered">
    <thead>
        <th><?= Html::encode(Yii::t('docvault', 'User')) ?></th>
        <th><?= Html::encode(Yii::t('docvault', 'Rights')) ?></th>
    </thead>
    <tbody>
        <?php foreach($model->permissions as $permission) : ?>
        <tr>
            <td><?= !is_null($permission->user) ? $permission->user->username : Yii::t('docvault','All Users') ?></td><td><?= /* $permission->rights . ' | ' . */ $permission->rightsOptions[$permission->rights] ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

