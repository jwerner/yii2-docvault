<?php
use yii\helpers\Html;
?>

<?= Yii::$app->timeZone ?>

<table class="table table-striped table-bordered">
    <thead>
        <th><?= Html::encode(Yii::t('docvault', 'User')) ?></th>
        <th><?= Html::encode(Yii::t('docvault', 'Check In Date')) ?></th>
        <th><?= Html::encode(Yii::t('docvault', 'Notes')) ?></th>
    </thead>
    <tbody>
        <?php foreach($model->logs as $log) : ?>
        <tr>
            <td><?= $log->user->username ?></td>
            <td><?= Yii::$app->formatter->asDatetime($log->modifiedOn .' '.'Europe/Berlin', 'medium') ?></td>
            <td><?= $log->note ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

