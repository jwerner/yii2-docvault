<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use diggindata\docvault\models\DocumentCategory;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-search-form" style="display:none">

<h3><?= Yii::t('docvault','Search Documents') ?></h3>

<div class="document-search row">

    <?php $form = \yii\bootstrap\ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'layout' => 'horizontal',
        'fieldConfig' => [
            //'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            //'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <div class="col-md-6">
    <?= $form->field($model, 'id', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('id')]])->label(false); ?>

    <?= $form->field($model, 'categoryId')->dropDownList(ArrayHelper::map(DocumentCategory::find()->all(), 'id', 'name'), ['prompt'=>Yii::t('docvault', '(Select a Category)')])->label(false) ?>

    <?= $form->field($model, 'ownerId')->dropDownList(ArrayHelper::map(\dektrium\user\models\User::find()->all(), 'id', 'username'), ['prompt'=>Yii::t('docvault', '(Select an Owner )')])->label(false) ?>
    </div>
    
    <div class="col-md-6">
    <?= $form->field($model, 'realname', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('realname')]])->label(false); ?>

    <?= $form->field($model, 'created', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('created')]])->label(false); ?>
    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('docvault', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('docvault', 'Reset'), ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('docvault', 'Show all'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    </div>
    <?php \yii\bootstrap\ActiveForm::end(); ?>

</div>

</div>
